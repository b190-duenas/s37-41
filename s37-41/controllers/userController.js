const User = require("../models/User.js");
const Course = require("../models/Course.js");
const auth = require("../auth.js");

const bcrypt = require("bcrypt");

/*
	1. use mongoose method "find" to find duplicate emails
	2. use .then method to send a response based on the result of the "find" method
*/

module.exports.checkEmailExists = ( reqBody ) => {
	return User.find( { email: reqBody.email } ).then( result => {
		// if there is an existing duplicate email
		if (result.length > 0){
			return true;
		}else{
		// if the user email is not yet registered in our database
			return false;
		}
	} )
};

// User Registration
/*
	1. create a new User object using the mongoose mode and the information from the request body
	2. Make sure that the password is encrypted
	3. save the object to the database
*/

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// hashSync - bcrypt's method for encrypting the password of the user once they have successfully registered in our database
			/*
				first parameter - the value to which the encryption will be done -  password coming from the request body

				"10" - it dictates how may "salt" rounds are to be given to encrypt the value
			*/
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) =>{
		if (error) {
			return false;
		}else{
			return true;
		}
	})
};

// User Login
/*
	1. check the database of the user email exists
	2. compare the password provided in the request body with the password stored in the database
	3. generate/return a JSON web token if the user hasd successfully logged in and return false if not
*/
module.exports.loginUser = ( reqBody ) => {
	return User.findOne( { email: reqBody.email } ).then(result =>{
		// if the user email does not exist
		if(result === null){
			return false;
		// if the user email exists in the database
		} else {
			// compareSync = decodes the encrypted password from the database and compares it to the password received from the request body
			// it's a good that if the value returned by a method/function is boolean, the variable name should be answerable by yes/no
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result) }
			}else{
				return false;
			}
		}
	} )
} 

// S38 - Activity
// Retrieve user details
	/*
		Steps:
		1. Find the document in the database using the user's ID
		2. Reassign the password of the returned document to an empty string
		3. Return the result back to the frontend
	*/
module.exports.getProfile = (data) => {
	console.log(data);
	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;
	});
};

module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId : data.courseId});

		return user.save().then((user,error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		})
	})

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId: data.userId});

		return course.save().then((course,error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		})
	})

	if ((isUserUpdated && isCourseUpdated)) {
		return true ;
	} else {
		return false;
	}
}